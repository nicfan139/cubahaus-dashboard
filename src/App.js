import React, { Component } from 'react'
import './App.css';
import axios from 'axios';
import { LoginPanel } from './pages/login';
import { Dashboard } from './pages/dashboard';
import { RoomView } from './pages/room';


class App extends Component {
  constructor() {
    super();
    this.state = {
      page: "",
      eventname: "",
      viewRoomDetails: null,
    }
  }

  componentWillMount() {
    const storedUserToken = localStorage.getItem("ch-userToken");
    if (storedUserToken) {
      this.setState({ page: "dashboard" })
    } else {
      this.setState({ page: "login" })
    }
  }


  handleLogin = (username, email, password) => {
    console.log(username);
    console.log(email);
    console.log(password);
    const authObj = {
      "username": username,
      "email": email,
      "password": password
    }
    console.log(authObj);

    fetch("https://cubahaus-api.herokuapp.com/accounts/login/", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(authObj)
    }).then(response => response.json())
    .then(data => {
      console.log(data);
      localStorage.setItem("ch-username", data["user"]["username"])
      localStorage.setItem("ch-userToken", data["token"])
      this.setState({
        page: "dashboard"
      })
    })
  }

  // handleClick = () => {
  // axios.get('https://cubahaus-api.herokuapp.com/')
  //     .then(response => this.setState({eventname: response.data.name}))

  // }


  viewRoom = (roomURL) => {
    // Get user credentials from localStorage
    const username = localStorage.getItem("ch-username");
    const userToken = localStorage.getItem("ch-userToken");

    // GET request to Cubahaus API
    fetch(`https://cubahaus-api.herokuapp.com${roomURL}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        "Authorization": `JWT ${userToken}`
      }
    }).then(response => response.json())
    .then(data => {
      this.setState({
        page: "room",
        viewRoomDetails: data
      });
    })
  }


  viewDashboard = () => {
    this.setState({ page: "dashboard" })
  }


  handleLogout = () => {
    // POST request to 
    fetch("https://cubahaus-api.herokuapp.com/accounts/logout/", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      }
    }).then(response => response.json())
    .then(data => console.log(data))

    localStorage.clear();
    this.setState({ page: "login" });
  }


  render() {
    const { page, viewRoomDetails } = this.state;

    return (
      <div className='cubahaus-app'>
        {
          (() => {
            switch(page) {
              case "login": return <LoginPanel handleLogin={this.handleLogin}/>;
              case "dashboard": 
                return (
                  <Dashboard
                    viewRoom={this.viewRoom}
                    handleLogout={this.handleLogout}
                  />
                );
              case "room":
                return (
                  <RoomView
                    viewDashboard={this.viewDashboard}
                    roomDetails={viewRoomDetails}
                  />
                );
              default: return "App is loading...";
            }
          })()
        }
      </div>
    )
  }
}


export default App



